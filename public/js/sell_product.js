var downloadURL;
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {

            document.getElementById('input_form').innerHTML = '<main role="main" class="container"><form><div id = "myText" class="p-3 bg rounded box-shadow"><h5 class="border-bottom border-gray pb-2 mb-0">Product Name</h5><textarea class="form-control" rows="1" id="product_name" style="resize:none" required = "required"></textarea><h5 class="border-bottom border-gray pb-2 mb-0">Product Price</h5><input class="form-control" rows="1" id="product_price" style="resize:none" required = "required" type = "number"></input><h5 class="border-bottom border-gray pb-2 mb-0">Product Iroduction</h5><textarea class="form-control" rows="12" id="product_intro" style="resize:none" required = "required"></textarea><div class="media text-muted pt-3"><div id="post_btn" >Submit</div><input id = "file" type = "file" onchange = "setting()" required = "required"></div></div></form></main>';
             // At first, let's check if we have permission for notification
            // If not, let's ask for it
            if (Notification && Notification.permission !== "granted") {
              Notification.requestPermission(function (status) {
                if (Notification.permission !== status) {
                  Notification.permission = status;
                }
              });
            }
            user_email = user.email;
            var changed = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+ user_email +'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="BuyList.html">購物清單</a><a class="dropdown-item" href="SellList.html">銷售清單</a><a id = "btnCOL" class="dropdown-item" href="checkOutList.html">訂單查詢</a><a id = "btnLogout" class="dropdown-item" href="index.html">登出</a></div></li>';
            menu.innerHTML = changed;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btn_Logout = document.getElementById('btnLogout');
            btn_Logout.addEventListener("click",function(){
                firebase.auth().signOut().then(function() {
                    menu.innerHTML = '<a id = "dynamic-menu" class="nav-item nav-link" href="signin.html">Login</a>';
                    console.log("User sign out!");
                }, function(error) {
                    console.log("User sign out error!");
                })
            },false);
              //text_upload
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('product_intro');
    post_name = document.getElementById('product_name');
    post_price = document.getElementById('product_price');
    var postsRef = firebase.database().ref('com_list');
    post_btn.addEventListener('click', function () {
        var user = firebase.auth().currentUser;
        if (post_txt.value != "" && post_name != "" && post_price != "" && downloadURL != undefined) {
            console.log(downloadURL);
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var newPostKey = firebase.database().ref().child('com_list').push().key;
            var data = {
                email: user_email,
                name: post_name.value,
                data: post_txt.value, 
                price: post_price.value,
                URL: downloadURL,
                sellerID: user.uid,
                number: 0,
                key: newPostKey
            }
            var SellListRef = firebase.database().ref('sellList/' + user.uid);
            console.log(user.uid);
            firebase.database().ref('com_list/' + newPostKey).set(data);
            SellListRef.push(data);
            post_txt = "";
            post_name = "";
            post_price = "";
            //
            // If the user agreed to get notified
    if (Notification && Notification.permission === "granted") {
        var n = new Notification("Upload Product!");
      }
      // If the user hasn't told if he wants to be notified or not
      // Note: because of Chrome, we are not sure the permission property
      // is set, therefore it's unsafe to check for the "default" value.
      else if (Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
          // If the user said okay
          if (status === "granted") {
            var n = new Notification("Upload Product!");
          }
          // Otherwise, we can fallback to a regular modal alert
          else {
            alert("Upload Product!");
          }
        });
      }
      // If the user refuses to get notified
      else {
        // We can fallback to a regular modal alert
        alert("Upload Product!");
      }
            //
            //document.location.href = "sell_product.html";//為什麼不會轉，要做upload成功才能submit
            console.log('1');
        }
        

        
    });    
        
        } else {
            // It won't show any post if not login
            //menu.innerHTML = '<a class="nav-item nav-link navbar-nav collapse navbar-collapse" href="signin.html">Login</a>';
            //document.getElementById('post_list').innerHTML = "";
            document.location.href = "signin.html";
        }
    });

  

    

    



}

window.onload = function () {
   
    init();

}



function setting(){
    file = document.querySelector('#file').files[0];
    var filename = file.name;
        var storageRef = firebase.storage().ref('/myProduct/'+filename);
        var uploadTask = storageRef.put(file);

        // Register three observers:
        // 1. 'state_changed' observer, called any time the state changes
        // 2. Error observer, called on failure
        // 3. Completion observer, called on successful completion
        uploadTask.on('state_changed', function(snapshot){
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
        }, function(error) {
            // Handle unsuccessful uploads
        }, function() {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            downloadURL = uploadTask.snapshot.downloadURL;
            var post_btn = document.getElementById('post_btn');
            post_btn.innerHTML = '<button id = postbtn>Submit</button>'

        });
}


