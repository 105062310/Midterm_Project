function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            var changed = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+ user_email +'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="BuyList.html">購物清單</a><a class="dropdown-item" href="SellList.html">銷售清單</a><a id = "btnCOL" class="dropdown-item" href="checkOutList.html">訂單查詢</a><a id = "btnLogout" class="dropdown-item" href="index.html">登出</a></div></li>';
            menu.innerHTML = changed;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btn_Logout = document.getElementById('btnLogout');
            btn_Logout.addEventListener("click",function(){
                firebase.auth().signOut().then(function() {
                    menu.innerHTML = '<a id = "dynamic-menu" class="nav-item nav-link" href="signin.html">Login</a>';
                    console.log("User sign out!");
                }, function(error) {
                    console.log("User sign out error!");
                })
            },false);
            
        
        } else {
            // It won't show any post if not login
            //menu.innerHTML = '<a class="nav-item nav-link navbar-nav collapse navbar-collapse" href="signin.html">Login</a>';
            //document.getElementById('post_list').innerHTML = "";
            document.location.href = "signin.html";
        }
    });

    // The html code for post
    var str_before_username = '<div id = "product" ><h5 class="card-title">';
    //var str_after_content = '<button id = "btnBuy" class="btn btnBuy">Buy</button></p></p></div>';
    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                //console.log(user_email);
                //var data_changed, name_changed;
                var childData = childSnapshot.val();
                /*console.log((childData.data).length);
                data_changed = (childData.data).trim();
                var i = 0, strLength = data_changed.length;
                for(i; i < strLength; i++) {
                data_changed = data_changed.replace(" ", "_");
                }
                console.log(data_changed);
                name_changed = (childData.name).trim();
                var j = 0, strLengthN = name_changed.length;
                for(j; j < strLengthN; j++) {
                name_changed = name_changed.replace(" ", "_");
                }
                console.log(childData.data);*/
                var str_after_content = '<button id = "btnQV" class="btn btnQV" data-toggle="modal" data-target="#myModal" onclick = Modal("' + childSnapshot.key+'")>Quick View</button></p></p></div>';
                var img = '<img id = "product_img" src = "' + childData.URL + '" height = "300vh" width = "280vw">';
                total_post[total_post.length] = str_before_username + img  + '</h5><p class="card-text"><h5>' +  childData.name  + '</h5><p id = "price">$' + childData.price  + '</p>' + str_after_content;
                first_count += 1;

            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                var childData = data.val();
                var data_changed, name_changed;
                second_count += 1;
                /*data_changed = (childData.data).trim();
                console.log(data_changed.length);
                var i = 0, strLength = data_changed.length;
                for(i; i < strLength; i++) {
                data_changed = data_changed.replace(" ", "_");
                }
                console.log(data_changed);
                name_changed = (childData.name).trim();
                var j = 0, strLengthN = name_changed.length;
                for(j; j < strLengthN; j++) {
                name_changed = name_changed.replace(" ", "_");
                }
                console.log(childData.name);*/
                if (second_count > first_count) {
                    var str_after_content = '<button id = "btnQV" class="btn btnQV" data-toggle="modal" data-target="#myModal" onclick = Modal("'+ data.key + '")>Quick View</button></p></p></div>';
                    var img = '<img id = "product_img" src = "' + childData.URL + '" height = "300vh" width = "280vw">';
                    total_post[total_post.length] = str_before_username + img + '</h5><p class="card-text"><h5>' + childData.name  + '</h5><p id = "price">$' + childData.price  + '</p>' + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}

function Modal(key){
    var ModalRef = firebase.database().ref('com_list/' + key);
    ModalRef.once('value').then(function(snapshot){
        var childData = snapshot.val();
        var string_before_PN = '<div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle">';
        var img = '<img id = "product_img" src = "' + childData.URL + '" height = "300px" width = "270px">';
        var string_after_PN = '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        var string_body = '<div class="modal-body container"><div class="row"><div class="col">';
        var string_after_body = '</div><div class="modal-footer" id = "footer"><p id = "price_modal">$';
        var string_after_price = '<input id = "number' + childData.key +'" type="number" name="quantity" min="1" max="10" value = "1" class = "number_modal"><button id = "buy_modal" type="button" class="btn btn-primary" onclick = BuyList("' + childData.key + '")>ADD TO CART</button></div></div></div>';
        document.getElementById('myModal').innerHTML = string_before_PN + childData.name + string_after_PN + string_body + img +'</div><div class="col"><p>商品型號：' + childData.key +'<br>詳細介紹：' + childData.data + '</p></div></div>' + string_after_body + childData.price + '</p>' + string_after_price;
    })
}

function BuyList(key){
    var user = firebase.auth().currentUser;
    var ID = user.uid;
    var BuyListRef = firebase.database().ref('buyList/' + ID);
    var number = document.getElementById('number' + key).value;
    var already_have = 0;
    BuyListRef.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                if(childData.key == key){//改key
                    var n = parseInt(childData.number);
                    console.log(childData.number);
                    n += parseInt(number);
                    already_have = 1;
                    BuyListRef.child(childSnapshot.key).update({number: n});
                    
                    alert('you bought ' + n + ' ' + childData.name + '.');
                }
            })
            if(already_have == 0){
                var comListRef = firebase.database().ref('com_list/' + key);
                comListRef.once('value').then(function(snapshot){
                    var Data = snapshot.val();
                    var data = {
                        email: Data.email,
                        name: Data.name,
                        price: Data.price,
                        number: number,
                        URL: Data.URL,
                        key: Data.key
                    }
                    BuyListRef.push(data);
                    alert('you bought ' + number + ' ' + Data.name + '.');
                })
                
            }
        })
    .catch(e => console.log(e.message));

}



