function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
       
        if (user) {
            var User = firebase.auth().currentUser;
            var ID = User.uid;
            console.log(ID);
            user_email = user.email;
            var changed = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+ user_email +'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="BuyList.html">購物清單</a><a class="dropdown-item" href="SellList.html">銷售清單</a><a id = "btnCOL" class="dropdown-item" href="checkOutList.html">訂單查詢</a><a id = "btnLogout" class="dropdown-item" href="index.html">登出</a></div></li>';
            menu.innerHTML = changed;
            var btn_Logout = document.getElementById('btnLogout');
            btn_Logout.addEventListener("click",function(){
                firebase.auth().signOut().then(function() {
                    menu.innerHTML = '<a id = "dynamic-menu" class="nav-item nav-link" href="signin.html">Login</a>';
                    console.log("User sign out!");
                }, function(error) {
                    console.log("User sign out error!");
                })
            },false);

             // The html code for post
            var str_at_begining = '<tr><th scope="row"><img id = "cart" src = "img/cart.png" height = "30px" width = "30px"></th><td>';
            var postsRef = firebase.database().ref('buyList/' + ID);
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;

            var totalMoney = 0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        console.log(childData.key);
                        var img = "<img id = 'product_img'src='" +  childData.URL + "' class='mr-2 rounded' style='height:150px; width:130px;'>";
                        var delete_btn = '<button id = "delete" type="button" class="btn btn-danger" onclick = DeleteProduct("'+ childSnapshot.key +  '")>Delete</button>';
                        total_post[total_post.length] = str_at_begining + img + '</td><td><h5>' + childData.name + '</h5>商品型號：' + childData.key + '<br>' + delete_btn +'</td><td>' + childData.price + '</td><td>' + childData.number + '</td><td>' + childData.price*childData.number;
                        totalMoney += childData.price*childData.number;
                        first_count += 1;

                    });
                    document.getElementById('total_money').innerHTML = '$' + totalMoney;
                    document.getElementById('subtotal').innerHTML = '$' + totalMoney;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            var img = "<img id = 'product_img' src='" +  childData.URL + "' class='mr-2 rounded' style='height:150px; width:130px;'>";
                            var delete_btn = '<button id = "delete" type="button" class="btn btn-danger" onclick = DeleteProduct("'+ childSnapshot.key +  '")>Delete</button>';
                            total_post[total_post.length] = str_at_begining + img + '</td><td><h5>' + childData.name + '</h5>商品型號：' + childData.key + '<br>' + delete_btn +'</td><td>' + childData.price + '</td><td>' + childData.number + '</td><td>' + childData.price*childData.number;
                            totalMoney += childData.price*childData.number;
                            document.getElementById('total_money').innerHTML = '$' + totalMoney;
                            document.getElementById('subtotal').innerHTML = '$' + totalMoney;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));

                btn_checkout.addEventListener('click', function(){
                    var user = firebase.auth().currentUser;
                    var ID = user.uid;
                    var BuyListRef = firebase.database().ref('buyList/' + ID);
                    var checkOutListRef = firebase.database().ref('checkoutList/' + ID);
                    var SellListRef = firebase.database().ref('sellList');
                    var name  = document.getElementById('inputName').value;
                    var address = document.getElementById('inputAddress').value;
                    var phone = document.getElementById('inputTel').value;
                    var payWay = document.getElementById('inputPay').value;
                    var email4 = document.getElementById('inputEmail4').value;
                    if(name != "" & address != "" & phone != "" & payWay != "" & email4 != ""){
                    BuyListRef.once('value')
                        .then(function(snapshot){
                            snapshot.forEach(function(childSnapshot){
                                var childData = childSnapshot.val();
                                checkOutListRef.push(childData);
                                console.log(childData);
                                var key = childData.key;
                                var n = parseInt(childData.number);
                                SellListRef.once('value')
                                    .then(function(snapshot){
                                        snapshot.forEach(function(childSnapshot){
                                            var ref = childSnapshot.ref;
                                            console.log("haha" +childSnapshot.val());
                                            ref.orderByChild("key").equalTo(key).once('value')
                                            .then(function(snapshot){
                                                snapshot.forEach(function(childSnapshot){
                                                    console.log(childSnapshot.val());
                                                    var childData_sellList = childSnapshot.val();
                                                    console.log(childData_sellList);
                                                    var ans_ref = childSnapshot.ref;
                                                    n += parseInt(childData_sellList.number);
                                                    ans_ref.update({"number":n});
                                                    var customer = {
                                                        name: name,
                                                        email: email4,
                                                        phone: phone,
                                                        address: address,
                                                        payWay: payWay,
                                                        quantity: childData.number
                                                    };
                                                    ans_ref.child('/customer').push(customer);
                                                })
                                            })
                                        })
                                    })
                                    .catch(e => console.log(e.message));
                            })
                            alert('訂單已送出');
                            firebase.database().ref(BuyListRef).remove().then(function(){
                                init();
                                console.log("checkout");
                            })
                        })
                        .catch(e => console.log(e.message));
                        
                    }
                    
                });
        } else {
            // It won't show any post if not login
            //menu.innerHTML = '<a class="nav-item nav-link navbar-nav collapse navbar-collapse" href="signin.html">Login</a>';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
}


function DeleteProduct(key){
    var user = firebase.auth().currentUser;
    var ID = user.uid;
    var ref = firebase.database().ref("buyList/" + ID + '/' + key);
    firebase.database().ref(ref).remove().then(function(){
        alert('Delete successfully.');
        init();
        console.log("delete");
    })
    
}





