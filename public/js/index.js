function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            var changed = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+ user_email +'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="BuyList.html">購物清單</a><a class="dropdown-item" href="SellList.html">銷售清單</a><a id = "btnCOL" class="dropdown-item" href="checkOutList.html">訂單查詢</a><a id = "btnLogout" class="dropdown-item" href="index.html">登出</a></div></li>';
            menu.innerHTML = changed;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btn_Logout = document.getElementById('btnLogout');
            btn_Logout.addEventListener("click",function(){
                firebase.auth().signOut().then(function() {
                    menu.innerHTML = '<a id = "dynamic-menu" class="nav-item nav-link" href="signin.html">Login</a>';
                    console.log("User sign out!");
                }, function(error) {
                    console.log("User sign out error!");
                })
            },false);
            
        
        } else {
            // It won't show any post if not login
            //menu.innerHTML = '<a class="nav-item nav-link navbar-nav collapse navbar-collapse" href="signin.html">Login</a>';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
}
