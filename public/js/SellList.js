function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
       
        if (user) {
            var User = firebase.auth().currentUser;
            var ID = User.uid;
            console.log(ID);
            user_email = user.email;
            var changed = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+ user_email +'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="BuyList.html">購物清單</a><a class="dropdown-item" href="SellList.html">銷售清單</a><a id = "btnCOL" class="dropdown-item" href="checkOutList.html">訂單查詢</a><a id = "btnLogout" class="dropdown-item" href="index.html">登出</a></div></li>';
            menu.innerHTML = changed;
            var btn_Logout = document.getElementById('btnLogout');
            btn_Logout.addEventListener("click",function(){
                firebase.auth().signOut().then(function() {
                    menu.innerHTML = '<a id = "dynamic-menu" class="nav-item nav-link" href="signin.html">Login</a>';
                    console.log("User sign out!");
                }, function(error) {
                    console.log("User sign out error!");
                })
            },false);

             // The html code for post
            
            var postsRef = firebase.database().ref('sellList/' + ID);
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;
            var totalMoney = 0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        console.log(childSnapshot.key);
                        var str_at_begining = '<tr><th scope="row"><img id = "trash" src = "img/Trash_can.png" height = "30px" width = "30px" onclick = DeleteProduct("'+ childSnapshot.key + '","'  + childData.key +'")></th><td>';
                        var img = "<img id = 'product_img'src='" +  childData.URL + "' class='mr-2 rounded' style='height:150px; width:130px;'>";
                        var detail_btn = '<button id = "detail" type="button" class="btn btn-danger" onclick = detail("'+ childSnapshot.key +'") data-toggle="modal" data-target="#customer_list">Detail</button>';
                        total_post[total_post.length] = str_at_begining + img + '</td><td><h5>' + childData.name + '</h5>商品型號：' + childData.key + '<br>' + detail_btn +'</td><td>' + childData.price + '</td><td>' + childData.number + '</td><td>' + childData.price*childData.number;
                        totalMoney += childData.price*childData.number;
                        first_count += 1;

                    });
                    document.getElementById('total_money').innerHTML = '$' + totalMoney;
                    document.getElementById('subtotal').innerHTML = '$' + totalMoney;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            var str_at_begining = '<tr><th scope="row"><img id = "trash" src = "img/Trash_can.png" height = "30px" width = "30px" onclick = DeleteProduct("'+ childSnapshot.key + '","'  + childData.key +'")></th><td>';
                            var img = "<img id = 'product_img' src='" +  childData.URL + "' class='mr-2 rounded' style='height:150px; width:130px;'>";
                            var delete_btn = '<button id = "delete" type="button" class="btn btn-danger" onclick = detail("'+ childSnapshot.key +'")>Detail</button>';
                            total_post[total_post.length] = str_at_begining + img + '</td><td><h5>' + childData.name + '</h5>商品型號：' + childData.key + '<br>' + delete_btn +'</td><td>' + childData.price + '</td><td>' + childData.number + '</td><td>' + childData.price*childData.number;
                            totalMoney += childData.price*childData.number;
                            document.getElementById('total_money').innerHTML = '$' + totalMoney;
                            document.getElementById('subtotal').innerHTML = '$' + totalMoney;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));
        } else {
            // It won't show any post if not login
            //menu.innerHTML = '<a class="nav-item nav-link navbar-nav collapse navbar-collapse" href="signin.html">Login</a>';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
}


function DeleteProduct(post_key, product_key){
    var user = firebase.auth().currentUser;
    var ID = user.uid;
    var ref_sellList = firebase.database().ref("sellList/" + ID + '/' +post_key);
    var buyListRef = firebase.database().ref("buyList");
    var comListRef = firebase.database().ref("com_list");
    var checkoutListRef = firebase.database().ref("checkoutList");
    //sell_delete
    firebase.database().ref(ref_sellList).remove().then(function(){
        alert('Delete successfully.');
        init();
        console.log("delete");
    })
    //buy_delete
    buyListRef.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var ref = childSnapshot.ref;
                ref.once('value')
                    .then(function(snapshot){
                        console.log(snapshot);
                        snapshot.forEach(function(childSnapshot){
                            var childData = childSnapshot.val();
                            if(childData.key == product_key){
                                var ref = childSnapshot.ref;
                                console.log(ref);
                                ref.remove().then(function(){
                                    console.log("buy_list delete");
                                })
                            }
                            
                        })
                    })
            })
        })
        .catch(e => console.log(e.message));
    //com_delete
    comListRef.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                console.log(product_key);
                console.log(childSnapshot.key);
                if(childSnapshot.key == product_key){
                    var ref = childSnapshot.ref;
                    ref.remove().then(function(){
                        console.log("com_list delete");
                    })
                }
            })
        })
        .catch(e => console.log(e.message));

        //checkout_delete
        /*checkoutListRef.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var ref = childSnapshot.ref;
                ref.once('value')
                    .then(function(snapshot){
                        console.log(snapshot);
                        snapshot.forEach(function(childSnapshot){
                            var childData = childSnapshot.val();
                            if(childData.key == product_key){
                                var ref = childSnapshot.ref;
                                console.log(ref);
                                ref.remove().then(function(){
                                    console.log("check_list delete");
                                })//改成賣家移除之類的？
                            }
                            
                        })
                    })
            })
        })
        .catch(e => console.log(e.message));*/
    
}

function detail(key){
    var user = firebase.auth().currentUser;
    var ID = user.uid;
    var SellListRef = firebase.database().ref('sellList/' + ID + '/' + key + '/customer');
    console.log(SellListRef);
    var total_post = [];
    SellListRef.once('value')
        .then(function(snapshot){
            console.log(snapshot.val());
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                console.log(childData);
                total_post[total_post.length] = '<tr><th scope="row">'+ childData.name +'</th><td>'+ childData.quantity +'</td><td>'+ childData.phone +'</td><td>'+ childData.address +'</td><td>'+ childData.payWay +'</td></tr>';

            })
            document.getElementById('customer_list_content').innerHTML = total_post.join('');
        })
}



