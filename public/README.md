# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [購物網站]
* Key functions (add/delete)
    1. [登入/登出]
    2. [買東西]
    3. [購物清單]
    4. [RWD]
    5. [host on gitlab page]
* Other functions (add/delete)
    1. [Google 登入]
    2. [Chrome Notification]
    3. [CSS Animation]
    4. [賣東西]
    5. [銷售清單]
    6. [訂單查詢]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
 1. 此網站為會員制，若尚未登入，點選頁面會自動挑轉到登入畫面，進行登入動作。
 2. 同一個帳號可以同時擁有買家與賣家的身份，以下用買家與賣家兩個身份來做功能介紹。 


### **賣家**
* Sell Product：
    進入Sell Product，及會出現輸入表單，供賣家寫入愈銷售商品的名稱、售價、詳細介紹與圖片，當圖片上傳完成，Submit鍵變成綠色即可送出表單，進入View Product即可看到商品上架，而從銷售清單中則可看到商品的銷售情況以及買家資訊。

* 銷售清單：
    1. 銷售清單幫助買家追蹤商品動態，若買家送處訂單，即可從各項商品的Detail看到買家相關資訊，方便出貨進行。
    2. 點案商品前面的垃圾桶圖樣即可將商品下架，在View Product及各消費者的購物車中的相關資訊皆會被刪除。

### **買家**
 * 商品瀏覽與購物
    1. 在View Product頁面或是從首頁的Shop Now按鍵即可進入商品介面，瀏覽各項商品。
    2. 若看到有興趣的產品即可點選Quick View，得知商品詳細資訊，若欲購買，於彈出視窗選好數量，按下ADD TO CART即可將商品加入購物車。
    3. 若重複點選相同商品，於購物車中只會做該項商品的數量增減。
 
 
 
 * 購物車
    1. 就是網頁中的購物清單。
    2. 點選購物清單即可看到自己剛剛加入的商品與數量，列表下方則會顯示當前購買商品的總金額。
    3. 若反悔不想買了，可以點選Delete按鈕，將商品從購物車清除。
    4. 若下定決心要購買，按下表單底部的灰色checkout按鈕進行結帳。
 
 * 結帳
    1. 從購物清單按下checkout按鈕即可進行結帳。
    2. 於彈出視窗中，填寫相關聯絡資料，讓賣家方便出貨。
    3. 點選送出之後，購物清單將會清空，可至訂單查詢當中查閱訂單資訊。
## Security Report (Optional)
* 利用各個頁面的js，判斷是否有登入，若有方進行頁面顯示，若無則直接跳轉到登入畫面。
* 利用各個使用者的ID當作資料夾名稱，於Database創造各個使用者的List，如此一來各使用者只可讀取到自己的購物清單、銷售清單、訂單資料。